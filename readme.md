# Welcome to Test Task!

This is a sample test task created for clients to prove our capabilities in PHP, Laravel and other libraries. The requirements were provided by the clients including database access and we implemented as per the steps mentioned.


# Files

We developed following main files.

## Controllers

- PlanController.php

## Models

- Plan.php
- PlanFeature.php
- PlanType.php


# Steps to deploy and run

- Clone this repo `git clone <this repo>`
- Open the terminal and `cd` to this folder
- Make sure the `php` and `composer` is installed
- Run `composer install`