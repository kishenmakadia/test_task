/// <reference path='ss-core.d.ts' />
(function (root, f) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], f);
    } else if (typeof exports === 'object') {
        module.exports = f(require('jquery'));
    } else {
        f(root.jQuery);
    }
})(this, function ($) {

    if (!window.ss) { window.ss = {}; }
    ss.icons = { spinner: '/www/img/svg/loading-button.svg', overlay: '/www/img/svg/loading-module.svg' };
    ss.templateURL = '/www/js/data/templates.js';
    ss.post = function (url, data, cache = 0) {
        var opt = {
            type: 'POST',
            url: url,
            data: (typeof data === 'string' ? data : JSON.stringify(data)),
            accept: 'application/json',
            dataType: 'json'
        }
        return ajaxCache(opt, cache);
    }
    ss.browserAgent = function () {
        return new UAParser().getResult();
    }
    ss.get = function (url, cache = 0) {
        var opt = {
            type: 'GET',
            url: url,
            accept: 'application/json',
            dataType: 'json'
        };
        return ajaxCache(opt, cache);
    }
    ss.upload = function (url, data) {
        return $.ajax({
            type: 'POST',
            url: url,
            data: data,
            rocessData: false,
            contentType: false,
            mimeTypes: 'multipart/form-data'
        });
    }
    ss.getView = function (url, target) {
        var $el = $(target);
        $el.loading('overlay', 'show');
        $.ajax(url).done(function (html) {
            $el.loading('overlay', 'hide');
            $el.html('<div name="content" style="display:none;"></div>').find('div:first').append(html).fadeIn(300);
        });
    }
    ss.appendCurrency = function (value) {
        return parseFloat(value).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1\'') + ' CHF';
    }
    ss.todate = function (value, type = 'date') {
        if (isString(value)) {
            var matches = value.match(/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/);
            if (matches === null) {
                return null;
            } else {
                if (type === 'date') {
                    var d = new Date(parseInt(matches[1]), parseInt(matches[2]) - 1, parseInt(matches[3]));
                    return pad(d.getDate()) + '.' + pad(d.getMonth() + 1) + '.' + d.getFullYear();
                }
                if (type === 'time') { return pad(parseInt(matches[4])) + ':' + pad(parseInt(matches[5])); }
            }
        }
        return null;
    }
    ss.jwtDecode = function () {
        try { return jwt_decode(ss.getCookie('token')); }
        catch (e) { if (e.name === 'InvalidTokenError') { return null; } }
    }
    ss.clearCache = function () {
        lscache.flush();
    }
    ss.setCache = function (key, item, cache = 0) {
        cache > 0 ? lscache.set(key, item, cache) : lscache.set(key, item);
    }
    ss.getCache = function (key) {
        return lscache.get(key);
    }
    ss.getCookie = function (name) {
        var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }
    ss.setCookie = function (name, value, days) {
        var d = new Date;
        d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
        document.cookie = name + '=' + value + ';path=/;expires=' + d.toGMTString();
    }
    ss.delCookie = function (name) {
        ss.setCookie(name, '', -1);
    }
    ss.urlHasParam = function (name, url = window.location.href) {
        return url.indexOf(name + '=') > 0
    }
    ss.urlQuery = function (name, url = window.location.href) {
        name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(url);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }
    ss.route = function (name, args) {
        var route = routes.find(x => x.name === name); // generated from laravel backend routes.js
        if (route !== undefined) {
            if (args !== undefined) { return route.url.replace(/\{(.*?)\}/g, function (i, match) { return args[match]; }); }
            return route.url
        }
    }
    ss.replace = function (str, args) {
        return Object.keys(args).reduce((p, c) => { return p.split('{' + c + '}').join(args[c]) }, str);
    }
    ss.detectLanguage = function () {
        var nav = window.navigator;
        var browserLanguagePropertyKeys = ['language', 'browserLanguage', 'systemLanguage', 'userLanguage'];
        var i, language;
        if (Array.isArray(nav.languages)) {
            for (i = 0; i < nav.languages.length; i++) {
                language = nav.languages[i];
                if (language && language.length) { return language.substr(0, 2); }
            }
        }
        for (i = 0; i < browserLanguagePropertyKeys.length; i++) {
            language = nav[browserLanguagePropertyKeys[i]];
            if (language && language.length) { return language.substr(0, 2); }
        }
        return 'en';
    }
    ss.notify = function (orig) {
        var opt = $.extend({}, orig, {
            text: orig.text || '',
            type: orig.type || 'success',
            target: orig.target || 'page',
            action: orig.action || null
        });
        toastr.options = {
            'closeButton': false,
            'preventDuplicates': true,
            'showEasing': 'linear',
            'hideEasing': 'linear',
            'progressBar': true,
            'showDuration': 200,
            'hideDuration': 200,
            'extendedTimeOut': 0,
        }
        if (opt.target === 'page') {
            toastr.options.preventDuplicates = false;
            toastr.options.newestOnTop = true;
            toastr.options.positionClass = ss.viewport.is('sm') ? 'toast-top-full-width' : 'toast-top-right';
            toastr.options.showMethod = ss.viewport.is('sm') ? 'slideDown' : 'fadeIn';
            toastr.options.hideMethod = ss.viewport.is('sm') ? 'slideUp' : 'fadeOut';
            toastr.options.timeOut = 3000;
            toastr.options.target = 'body';
        }

        if (opt.target === 'modal') {
            toastr.options.positionClass = 'toast-top-full-width';
            toastr.options.showMethod = 'slideDown';
            toastr.options.hideMethod = 'slideUp';
            toastr.options.timeOut = 2000;
            toastr.options.target = '.modal-content';
        }
        if (opt.action === 'reload') {
            toastr.options.onHidden = function () {
                location.reload();
            }
        }
        if (opt.type === 'fail' || opt.type === 'error') {
            toastr.error(opt.text);
        }
        if (opt.type === 'success') {
            toastr.success(opt.text);
        }
    }
    $.fn.stars = function (value) {
        var $el = $(this);
        var rating = value || $(this).attr('data-rating');
        $el.find('.rating').each(function () {
            for (var i = 0; i < 5; i++) {
                $(this).append('<span class="stars"><figure class="' + (i < rating ? 'active fa fa-heart' : 'fa fa-heart-o') + '"></figure></span>');
            }
        });
    }
    $.fn.rating = function () {
        var $el = $(this);
        var ratingElement =
            '<span class="stars">' +
            '<i class="fa fa-heart-o s1" data-score="1"></i>' +
            '<i class="fa fa-heart-o s2" data-score="2"></i>' +
            '<i class="fa fa-heart-o s3" data-score="3"></i>' +
            '<i class="fa fa-heart-o s4" data-score="4"></i>' +
            '<i class="fa fa-heart-o s5" data-score="5"></i>' +
            '</span>';
        $el.find('.star-rating').each(function () {
            $(this).append(ratingElement);
            if ($(this).hasClass('active')) {
                $(this).append('<input readonly hidden="" name="score_' + $(this).attr('data-name') + '" id="score_' + $(this).attr('data-name') + '">');
            }
            // If rating exists
            for (var rate = 0; rate < $(this).attr('data-rating'); rate++) {
                $(this).children('.stars').children('.s' + rate + 1).addClass('active');
            }
        });
        $el.find('.star-rating.active i')
            .mouseenter(function () {
                for (var i = 0; i < $(this).attr('data-score'); i++) {
                    var a = i + 1;
                    $(this).parent().children('.s' + a).addClass('hover');
                }
            })
            .mouseleave(function () {
                for (var i = 0; i < $(this).attr('data-score'); i++) {
                    var a = i + 1;
                    $(this).parent().children('.s' + a).removeClass('hover');
                }
            });
        $el.find('.star-rating.active i')
            .on('click', function () {
                $(this).parents('.star-rating').find('input').val($(this).attr('data-score'));
                $(this).parent().children('.fa').removeClass('active');
                for (var i = 0; i < $(this).attr('data-score'); i++) {
                    var a = i + 1;
                    $(this).parent().children('.s' + a).addClass('active');
                }
                return false;
            });
    }
    $.fn.applyRemoteErrors = function (errors) {
        var $el = $(this);
        $.each(errors, function (name) {
            $el.find('.form-control[name="' + name + '"]').each(function () {
                $(this).attr('data-remote-validation', errors[name][0]);
                $(this).rules('add', 'server');
                $(this).validate();
                $(this).one('change', function () {
                    $(this).rules('remove', 'server');
                    $(this).validate();
                    $el.valid();
                });
            });
        });
    }
    $.fn.clearRemoteErrors = function () {
        var $el = $(this);
        $el.find('.form-control[data-remote-validation]').each(function () {
            $(this).rules('remove', 'server');
            $(this).validate();
        });
    }
    $.fn.applyTemplate = function (orig) {
        var deferred = $.Deferred();
        var $el = $(this);
        var opt = $.extend({}, orig, {
            url: orig.url,
            mode: orig.mode,
            template: orig.template,
            wrap: orig.wrap === undefined ? true : orig.wrap,
            cache: orig.cache === undefined ? false : orig.cache,
            overlay: orig.overlay === true ? $el : undefined
        });
        if (opt.cache && opt.mode === 'replace' && $el.data('timestamp')) {
            deferred.resolve({ html: $el, url: null, data: null });
        } else {
            if (opt.overlay) {
                $el.loading('overlay', 'show');
            }
            var p1 = $.ajax({
                url: ss.templateURL,
                dataType: 'script',
                cache: true
            });
            var p2 = $.ajax(opt.url);
            $.when(p1, p2).done(function (script, json) {
                var html = Handlebars.templates[opt.template](json[0].data);
                if (orig.overlay) {
                    $el.loading('overlay', 'hide');
                }
                if (opt.mode === 'replace') {
                    if (opt.wrap) {
                        $el.data('timestamp', Date.now());
                        $el.html('<div name="content" style="display:none;"></div>').find('div:first').append(html).fadeIn(300);
                    } else {
                        $el.html(html).hide().fadeIn(300);
                    }
                }
                if (opt.mode === 'append') {
                    $el.append(html);
                }
                deferred.resolve({
                    html: $el,
                    url: json[0].url,
                    data: json[0].data
                });
            });
        }
        return deferred.promise();
    }
    $.fn.applyValues = function (map) {
        return this.each(function () {
            var $el = $(this);
            $.each(map, function (k, v) {
                var d = ss.todate(v, 'date');
                $el.find('#' + k + ',[name=' + k + ']').val(d !== null ? d : v);
            });
            $el.find('[data-html]').each(function () {
                $(this).html(map[$(this).data('html')] || '');
            });
            $el.find('[data-val]').each(function () {
                $(this).val(map[$(this).data('val')] || '');
            });
            $el.find('[data-src]').each(function () {
                $(this).attr('src', map[$(this).data('src')] || '');
            });
            $el.find('[data-href]').each(function () {
                $(this).attr('href', map[$(this).data('href')] || '');
            });
        });
    }
    $.fn.loading = function (type = 'button', status = 'show') {
        var $el = $(this);
        if (type === 'button' && status === 'show') {
            $el.data('loading', $el.text())
                .attr('style', 'padding: 0px; border: 0px; min-height: ' + $el.outerHeight() + 'px;')
                .prop('disabled', true)
                .html('<img src="' + ss.icons.spinner + '"/>');
        }
        if (type === 'button' && status === 'hide') {
            $el.text($el.data('loading')).removeAttr('data-loading style disabled');
        }
        if (type === 'overlay' && status === 'show') {
            var overlay = $('<div data-type="overlay"></div>');
            overlay.css({
                position: 'absolute',
                width: '100%',
                height: '100%',
                left: 0,
                top: 0,
                'min-height': '200px',
                background: 'url(' + ss.icons.overlay + ') center center no-repeat'
            });
            $el.empty();
            overlay.hide();
            $el.append(overlay);
            overlay.fadeIn();
        }
        if (type === 'overlay' && status === 'hide') {
            $el.filter('[data-type="overlay"]').remove();
        }
    }
    $.fn.ajaxSubmit = function () {
        var deferred = $.Deferred();
        var $form = $(this);
        var $btn = $(this).find('button[type=submit]');
        $form.clearRemoteErrors();
        if (!$form.valid()) {
            deferred.reject();
        } else {
            $btn.loading('button', 'show');
            var opt = {
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize(),
                accept: 'application/json',
                dataType: 'json',
                headers: { 'Authorization': 'Bearer ' + ss.getCookie('jwt') }
            };
            $.ajax(opt)
                .done(function (xhr) {
                    deferred.resolve(xhr);
                })
                .fail(function (xhr) {
                    if (xhr.type === 'validation') {
                        $form.applyRemoteErrors(xhr.responseJSON);
                        $form.valid();
                    }
                    deferred.resolve(xhr.responseJSON);
                })
                .always(function () {
                    $btn.loading('button', 'hide');
                });
        }
        return deferred.promise();
    }

    function ajaxCache(opt, ttl = 0) {
        var deferred = $.Deferred();
        if (ttl) {
            var key;
            if (opt.type === 'POST') { key = opt.url + '?' + hash(JSON.stringify(opt.data)) } else { key = opt.url; }
            var value = lscache.get(key);
            if (value !== null) { return deferred.resolve(value); }
        }
        $.ajax(opt)
            .done(function (xhr) {
                ttl > 0 ? lscache.set(opt.url, xhr, ttl) : lscache.set(opt.url, xhr);
                deferred.resolve(xhr);
            })
            .fail(function (xhr) { deferred.resolve(xhr.responseJSON); });

        return deferred.promise();
    }

    function hash(s) {
        var hash = 0,
            strlen = s.length,
            i, c;
        if (strlen === 0) { return hash; }
        for (i = 0; i < strlen; i++) {
            c = s.charCodeAt(i);
            hash = ((hash << 5) - hash) + c;
            hash = hash & hash;
        }
        return hash;
    }

    function isString(value) {
        return typeof value === 'string' || value instanceof String;
    }

    function pad(d) {
        return d < 10 ? '0' + d : d;
    }

    return ss;
});