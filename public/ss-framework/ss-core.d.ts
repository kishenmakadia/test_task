declare namespace ss {

    let icons: Icon;

    let templateURL: string;

    function browserAgent(): any;

    function post(url: string, data: Object, { cache = 0 }: { cache?: int }): any;

    function get(url: string, { cache = 0 }: { cache?: int }): any;

    function upload(url: string, data: Object): any;

    function getView(url: string, target: string): any;

    function appendCurrency(value: string): string;

    function todate(value: string, { type = 'date' }: { type?: string }): Date;

    function jwtDecode(): any;

    function clearCache(): void;

    function setCache(key: string, item: string, { cache = 0 }: { cache?: int }): void;

    function getCache(key: string): string;

    function getCookie(name: string): string;

    function setCookie(name: string, value: string, days: int): void;

    function delCookie(name: string): void;

    function urlHasParam(name: string, { url = window.location.href }: { url?: string }): boolean;

    function urlQuery(name: string, { url = window.location.href }: { url?: string }): any;

    function route(url: string, ...args: any[]): string;

    function replace(str: string, ...args: any[]): string;

    function detectLanguage(): string;

    interface Icon {
        spinner: string;
        overlay: string;
    }
}

interface JQuery {
    stars: (value: number) => JQuery;
    rating: () => JQuery;
    applyRemoteErrors: (errors: Object[]) => JQuery;
    clearRemoteErrors: () => JQuery;
    applyTemplate: (orig: { url: string, mode: string, template: string, wrap: boolean, cache: boolean, overlay: boolean }) => Promise;
    applyValues: (map: { [index: string]: string }) => JQuery;
    loading: (type: string, { status = 'show' }: { status?: string }) => JQuery;
    notify: (orig: { [index: string]: string }) => JQuery;
    ajaxSubmit: () => Promise;
}

declare module 'ss-core' { export = ss; }