var data;
var handebar_mode = 'compile';
var handebar_mode = 'precompile';
var handebar_mode = 'using_custom_function';

$(document).ready(function(){

	//if not using custom function get data using ajax.
	if (handebar_mode != 'using_custom_function')
	{
		$.ajax({
			url: "api",
			dataType : 'json',
			success: function(result)
			{
				data = result;
				startTemplating();
			}
		});
	}
	else
	{
		startTemplating();
	}

	function startTemplating()
	{
		ss.templateURL = 'js/templates/templates.js';
		ss.icons.overlay = 'images/loader.gif';


		// Check string helper
		Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
			return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
		});

		// Math helper
		Handlebars.registerHelper("math", function(lvalue, operator, rvalue, options) {
			lvalue = parseFloat(lvalue);
			rvalue = parseFloat(rvalue);
				
			return {
				"+": ((lvalue + rvalue) % 1 == 0) ? (lvalue + rvalue) : parseFloat((lvalue + rvalue)).toFixed(2),
				"-": ((lvalue - rvalue) % 1 == 0) ? (lvalue - rvalue) : parseFloat((lvalue - rvalue)).toFixed(2),
				"*": ((lvalue * rvalue) % 1 == 0) ? (lvalue * rvalue) : parseFloat((lvalue * rvalue)).toFixed(2),
				"/": ((lvalue / rvalue) % 1 == 0) ? (lvalue / rvalue) : parseFloat((lvalue / rvalue)).toFixed(2),
				"%": ((lvalue % rvalue) % 1 == 0) ? (lvalue % rvalue) : parseFloat((lvalue % rvalue)).toFixed(2)
			}[operator];
		});

		// if condition helper 
		Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
			switch (operator) {
				case '==':
					return (v1 == v2) ? options.fn(this) : options.inverse(this);
				case '===':
					return (v1 === v2) ? options.fn(this) : options.inverse(this);
				case '!=':
					return (v1 != v2) ? options.fn(this) : options.inverse(this);
				case '!==':
					return (v1 !== v2) ? options.fn(this) : options.inverse(this);
				case '<':
					return (v1 < v2) ? options.fn(this) : options.inverse(this);
				case '<=':
					return (v1 <= v2) ? options.fn(this) : options.inverse(this);
				case '>':
					return (v1 > v2) ? options.fn(this) : options.inverse(this);
				case '>=':
					return (v1 >= v2) ? options.fn(this) : options.inverse(this);
				case '&&':
					return (v1 && v2) ? options.fn(this) : options.inverse(this);
				case '||':
					return (v1 || v2) ? options.fn(this) : options.inverse(this);
				default:
					return options.inverse(this);
			}
		});


		/*Tabs*/

		if (handebar_mode == 'using_custom_function')
		{
			$('#parsed-tabs').applyTemplate({
				url : "api",
				template : "tabs",
				mode : 'replace',
				cache : true,
				wrap : false,
				overlay : true,
			}).done(res => {
				console.log('res',res);
			});
		}
		else
		{
			var theTabTemplateScript = $("#tabs").html();
			
			if (handebar_mode == 'compile') 
			{
				var theTabTemplate = Handlebars.compile(theTabTemplateScript);
			}
			
			if (handebar_mode == 'precompile') 
			{
				var theTabTemplate = Handlebars.templates['tabs'];;
			}

			var theCompiledTabHtml = theTabTemplate({data:data});

			$("#parsed-tabs").html(theCompiledTabHtml);
		}



		/*Tab Content*/

		if (handebar_mode == 'using_custom_function') 
		{
			$('#parsed-tab-content').applyTemplate({
				url : "api",
				template : "tab-content",
				mode : 'replace',
				cache : true,
				wrap : false,
				overlay : true,
			}).done(res => {
				console.log('res',res);
			});
		}
		else
		{
			var theTabContentTemplateScript = $("#tab-content").html();
			
			if (handebar_mode == 'compile') 
			{
				var theTabContentTemplate = Handlebars.compile(theTabContentTemplateScript);
			}
			
			if (handebar_mode == 'precompile')
			{
				var theTabContentTemplate = Handlebars.templates['tab-content'];;
			}

			var theCompiledTabContentHtml = theTabContentTemplate({data:data});

			$("#parsed-tab-content").html(theCompiledTabContentHtml);
		}

		// hiding the loader after the templates are in place
		$('.loader').hide();

		//toggle the monthly-quarterly switch
		var switch_selector = "#switcher";

		$(document).on("click",switch_selector , function()
		{
			$("#filt-quarterly").toggleClass("toggler--is-active");
			$("#filt-monthly").toggleClass("toggler--is-active");
		});

		//checkbox trigger month-quarter
		$(document).on('change',switch_selector , function(){
			//if quarterly
			if ($(this).prop('checked')) 
			{
				$('.month-plan').addClass('hidden');
				$('.quarter-plan').removeClass('hidden');
			}
			else //if monthly
			{
				$('.month-plan').removeClass('hidden');
				$('.quarter-plan').addClass('hidden');
			}
		});

		// tooltip
		$(".tooltip-link").hover(function()
		{
			$(this).closest(".circle-animation-back").find(".toolip-box").show(500);
		});

		$(".tooltip-link").mouseleave(function()
		{
			$(this).closest(".circle-animation-back").find(".toolip-box").delay(1000).hide(500);
		});

		//buy plan
		$(".buy-now").on('click',function(){
			var plan_id = $(this).data('id');
			$.ajax({
				url: "api/buy-plan",
				method : 'post',
				data : {"plan_id": plan_id},
				dataType : 'json',
				success: function(result)
				{
					alert("Message from server : \n"+result.message);
				}
			});
		});
	}
});
