<script id="tabs" type="text/x-handlebars-template">
	<ul class="nav nav-tabs">
		@{{#each data as |type index|}}
			<li class="text-capitalize @{{#unless index}} active @{{/unless}}"><a data-toggle="tab" href="#page@{{type.id}}">@{{type.code}}</a></li>
		@{{/each}} 
	</ul>
</script>