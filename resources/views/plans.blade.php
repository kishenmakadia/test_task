<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
        <title>Plans</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700&subset=latin-ext" />
        
        <!-- css files -->
        <link rel="stylesheet" href="ss-framework/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="css/pricing-style.css" type="text/css" /> 

        <!-- custom css files -->
        <link rel="stylesheet" href="css/style.css" type="text/css" /> 
        
        <!-- js files -->
        <script src="ss-framework/jquery.js"></script>
        <script src="ss-framework/bootstrap.js"></script>

        <!-- if using compiled handlebar template with helper function -->
        <script src="ss-framework/handlebars.js"></script>
        <script src="ss-framework/ss-core.js"></script>

        {{-- using the pre-compiled handlebar template            
            <script src="js/templates/templates.js"></script> --}}
        
        {{-- compiling handlebars template at runtime
            <script src="ss-framework/handlebars-v4.0.11.js"></script>
            @include('partials.tabs')  
            @include('partials.tab-content')
         --}}
    </head>
    <body>
        <div class="loader">
            <img src="images/loader.gif" alt="loader">
        </div>

        <!-- container -->
        <div class="container">
            <h2>Plans</h2>
            <br>
            <div id='parsed-tabs'></div>
            <div id='parsed-tab-content'></div>
            <br>
        </div>
        
        <!-- custom js files -->
        <script src="js/app.js"></script>
    </body>
</html>