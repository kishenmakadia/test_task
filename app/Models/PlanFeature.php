<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanFeature extends Model
{
    /* attributes */
    public function getNameAttribute($value)
    {
        return json_decode($value);
    }

    /* scopes */
    public function scopeOrdered($query)
	{
	    return $query->orderBy('sort_order', 'asc');
	}
}
