<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    /* relations */
    public function plan_features()
    {
    	return $this->hasMany('App\Models\PlanFeature')->ordered();
    }

    /* attributes */
    public function getNameAttribute($value)
    {
        return json_decode($value);
    }

    public function getDescriptionAttribute($value)
    {
        return json_decode($value);
    }

    public function getPriceAttribute($value)
    {
        return (int)$value;
    }

    /* scopes */
    public function scopeOrdered($query)
	{
	    return $query->orderBy('sort_order', 'asc');
	}
}
