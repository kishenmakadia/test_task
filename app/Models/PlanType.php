<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanType extends Model
{
    protected $table = "plan_type";

    public function plans()
    {
    	return $this->hasMany('App\Models\Plan')->ordered();
    }

    public function getValueAttribute($value)
    {
        return json_decode($value);
    }
}
