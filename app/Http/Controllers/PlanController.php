<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PlanType;

class PlanController extends Controller
{
	/*plans listing*/
    public function index()
    {
	    return view('plans');
    }

	/*plans api*/
    public function api()
    {
    	$plan_types = PlanType::whereIn('code',['independent','company'])->with('plans.plan_features')->get();	
        // return json_encode(['data'=>$plan_types]);
    	return response()->json(['data'=>['data'=>$plan_types]]);
    }

	/*plans button*/
    public function buy_plan(Request $request)
    {
    	return json_encode(['message'=>"You have bought plan with id : ".$request->get('plan_id')]);
    }
}
